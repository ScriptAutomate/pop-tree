import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="tree")
    yield hub
