import json
import unittest.mock as mock

import tests.integration.mod.test

f = tests.integration.mod.test.__file__


def test_full(hub, capsys):
    with mock.patch("sys.argv", ["pop-doc", "mod.test", "--output=json"]):
        hub.tree.init.doc_cli()

    out, err = capsys.readouterr()
    ret = json.loads(out)
    assert ret == {
        "attributes": ["Any", "Class", "List", "MODULE_VAR", "module_func"],
        "classes": {
            "Class": {
                "attributes": ["CLASS_VAR", "method"],
                "doc": "Class doc",
                "end_line_number": 25,
                "file": f,
                "functions": {
                    "method": {
                        "doc": "method " "doc",
                        "end_line_number": 23,
                        "file": f,
                        "ref": "mod.test.Class.method",
                        "start_line_number": 21,
                    }
                },
                "ref": "mod.test.Class",
                "signature": {
                    "parameters": {
                        "arg": {"annotation": "<class " "'str'>"},
                        "kwarg": {
                            "annotation": "<class " "'bool'>",
                            "default": None,
                        },
                        "self": {},
                    }
                },
                "start_line_number": 15,
                "variables": {
                    "CLASS_VAR": {
                        "file": f,
                        "ref": "mod.test.Class.CLASS_VAR",
                        "start_line_number": 24,
                        "type": "int",
                        "value": 1,
                    }
                },
            }
        },
        "doc": "Test Module",
        "file": f,
        "functions": {
            "module_func": {
                "contracts": {
                    "call": ["mod.contracts.test.call"],
                    "post": [
                        "mod.contracts.test.post_module_func",
                        "mod.contracts.test.post",
                        "mod.recursive_contracts.test.post_module_func",
                        "mod.recursive_contracts.test.post",
                    ],
                    "pre": [
                        "mod.contracts.test.pre_module_func",
                        "mod.contracts.test.pre",
                        "mod.recursive_contracts.test.pre_module_func",
                        "mod.recursive_contracts.test.pre",
                    ],
                },
                "doc": "func doc",
                "end_line_number": 49,
                "file": f,
                "parameters": {
                    "arg": {"annotation": "<class " "'int'>"},
                    "hub": {},
                    "kwarg": {"annotation": "<class " "'str'>", "default": "taco"},
                    "list_of_dataclasses": {
                        "annotation": {
                            "typing.List[types.Tag]": {
                                "Key": {
                                    "annotation": "<class " "'str'>",
                                    "default": None,
                                },
                                "Value": {
                                    "annotation": "<class " "'str'>",
                                    "default": None,
                                },
                            }
                        },
                        "default": None,
                    },
                    "single_dataclass": {
                        "annotation": {
                            "<class 'types.Test'>": {
                                "Hello": {
                                    "annotation": "<class " "'str'>",
                                    "default": None,
                                },
                                "World": {
                                    "annotation": "<class " "'str'>",
                                    "default": None,
                                },
                            }
                        },
                        "default": None,
                    },
                },
                "ref": "mod.test.module_func",
                "return_annotation": "typing.Any",
                "start_line_number": 27,
            }
        },
        "ref": "mod.test",
        "variables": {
            "MODULE_VAR": {
                "file": f,
                "ref": "mod.test.MODULE_VAR",
                "start_line_number": 7,
                "type": "int",
                "value": 1,
            }
        },
    }


def test_class(hub, capsys):
    with mock.patch("sys.argv", ["pop-doc", "mod.test.Class", "--output=json"]):
        hub.tree.init.doc_cli()

    out, err = capsys.readouterr()
    ret = json.loads(out)
    assert ret == {
        "attributes": ["CLASS_VAR", "method"],
        "doc": "Class doc",
        "start_line_number": 15,
        "end_line_number": 25,
        "file": f,
        "functions": {
            "method": {
                "doc": "method doc",
                "end_line_number": 23,
                "file": f,
                "ref": "mod.test.Class.method",
                "start_line_number": 21,
            }
        },
        "ref": "mod.test.Class",
        "signature": {
            "parameters": {
                "arg": {"annotation": "<class 'str'>"},
                "kwarg": {"annotation": "<class 'bool'>", "default": None},
                "self": {},
            }
        },
        "variables": {
            "CLASS_VAR": {
                "file": f,
                "ref": "mod.test.Class.CLASS_VAR",
                "start_line_number": 24,
                "type": "int",
                "value": 1,
            }
        },
    }


def test_function(hub, capsys):
    with mock.patch("sys.argv", ["pop-doc", "mod.test.module_func", "--output=json"]):
        hub.tree.init.doc_cli()

    out, err = capsys.readouterr()
    ret = json.loads(out)
    assert ret == {
        "contracts": {
            "call": ["mod.contracts.test.call"],
            "post": [
                "mod.contracts.test.post_module_func",
                "mod.contracts.test.post",
                "mod.recursive_contracts.test.post_module_func",
                "mod.recursive_contracts.test.post",
            ],
            "pre": [
                "mod.contracts.test.pre_module_func",
                "mod.contracts.test.pre",
                "mod.recursive_contracts.test.pre_module_func",
                "mod.recursive_contracts.test.pre",
            ],
        },
        "doc": "func doc",
        "end_line_number": 49,
        "file": f,
        "parameters": {
            "arg": {"annotation": "<class 'int'>"},
            "hub": {},
            "kwarg": {"annotation": "<class 'str'>", "default": "taco"},
            "list_of_dataclasses": {
                "annotation": {
                    "typing.List[types.Tag]": {
                        "Key": {"annotation": "<class " "'str'>", "default": None},
                        "Value": {"annotation": "<class " "'str'>", "default": None},
                    }
                },
                "default": None,
            },
            "single_dataclass": {
                "annotation": {
                    "<class 'types.Test'>": {
                        "Hello": {"annotation": "<class " "'str'>", "default": None},
                        "World": {"annotation": "<class " "'str'>", "default": None},
                    }
                },
                "default": None,
            },
        },
        "ref": "mod.test.module_func",
        "return_annotation": "typing.Any",
        "start_line_number": 27,
    }


def test_variable(hub, capsys):
    with mock.patch("sys.argv", ["pop-doc", "mod.test.MODULE_VAR", "--output=json"]):
        hub.tree.init.doc_cli()

    out, err = capsys.readouterr()
    ret = json.loads(out)
    assert ret == {
        "file": f,
        "ref": "mod.test.MODULE_VAR",
        "start_line_number": 7,
        "type": "int",
        "value": 1,
    }
