"""Test Module"""
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import List

MODULE_VAR: int = 1


def __init__(hub):
    # TODO it should be visible
    hub.mod.test.INVISIBLE = 2


class Class:
    """Class doc"""

    def __init__(self, arg: str, kwarg: bool = None):
        """init doc"""

    def method(self, arg: bool, kwarg: Any) -> str:
        """method doc"""

    CLASS_VAR: int = 1


def module_func(
    hub,
    arg: int,
    kwarg: str = "taco",
    list_of_dataclasses: List[
        make_dataclass(
            "Tag",
            [
                ("Key", str, field(default=None)),
                ("Value", str, field(default=None)),
            ],
        )
    ] = None,
    single_dataclass: make_dataclass(
        "Test",
        [
            ("Hello", str, field(default=None)),
            ("World", str, field(default=None)),
        ],
    ) = None,
) -> Any:
    """func doc"""
