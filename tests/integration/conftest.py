import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture(scope="function")
def hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="tree")
    hub.pop.sub.add("tests.integration.mod", omit_class=False)
    with mock.patch("sys.exit"):
        yield hub
